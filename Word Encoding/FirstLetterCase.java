/* This code swaps the first letter of any word with its opposite case.
 * For example, 'hello' turns into 'Hello'.
 * If desired, this change can also be reversed with the decodeText() function
 */

package wordEncoding;

public class FirstLetterCase implements Word {

	public String user_text;

	public FirstLetterCase() {
	}

	// additional constructor for creating objects with text in Main

	public FirstLetterCase(String user_text) {
		this.user_text = user_text;
	}

	// getter and setter methods

	public void setText(String user_text) {
		this.user_text = user_text;
	}

	public String getText() {
		return user_text;
	}

	// prints updated text

	public void printText() {
		System.out.println(user_text);
	}

	public void encodeText() {

		// we isolate the zeroth character of the string and the remaining characters.

		char first_letter = user_text.charAt(0);
		String new_word_without_first_letter = user_text.substring(1, user_text.length());

		// we swap the first character to its opposite case

		if (Character.isLetter(first_letter) == true) {
			if (first_letter == Character.toUpperCase(first_letter)) {
				first_letter = Character.toLowerCase(first_letter);
			} else {
				first_letter = Character.toUpperCase(first_letter);
			}

			// set user_text as the new concatenation

			user_text = first_letter + new_word_without_first_letter;

			// just in case user enters a number, special character, or null

		} else {
			System.out.print("Sorry, but we cannot complete this request");
			System.out.println(" because the first character is not a letter.");
		}

	}

	public void decodeText() {

		// calling the encodeText method reverses the encoding

		encodeText();

	}
}