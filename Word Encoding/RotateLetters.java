package wordEncoding;

public class RotateLetters implements Word {

	public String user_text;

	public RotateLetters() {
	}

	// constructor for creating objects with text in Main

	public RotateLetters(String user_text) {
		this.user_text = user_text;
	}

	// getter and setter methods

	public void setText(String user_text) {
		this.user_text = user_text;
	}

	public String getText() {
		return user_text;
	}

	// prints updated text

	public void printText() {
		System.out.println(user_text);
	}

	public void encodeText() {

		String shift_the_letters = user_text;
		
		// concatenates user_text with its letters shifted one unit right (e.g. 'A' becomes 'B')

		for (int i = 0; i < user_text.length(); i++) {
			shift_the_letters += (char) (user_text.charAt(i) + 1);
		}
		
		// removes original user_text from concatenated string

		user_text = shift_the_letters.substring(shift_the_letters.length() / 2, shift_the_letters.length());

	}

	public void decodeText() {

	}
}