package wordEncoding;

import java.lang.String;
import java.util.ArrayList;
import java.util.Iterator;

public class WordDriver {

	public static void main(String[] args) {

		ArrayList<Word> word = new ArrayList<Word>(); // create an ArrayList of word objects

		word.add(new FirstLetterCase("test"));
		word.add(new FirstLetterCase("fun"));
		word.add(new FirstLetterCase("variable"));
		word.add(new FirstLetterCase("Lepinski"));
		word.add(new FirstLetterCase("Gillman"));
		word.add(new RotateLetters("Roy"));
		word.add(new RotateLetters("people"));
		word.add(new RotateLetters("music"));
		word.add(new RotateLetters("testing"));
		word.add(new L33T("assignment"));

		// Here we use an advanced for loop to iterate through the word objects
		// We also encode the text as we go through

		System.out.println("These are the words you entered: ");
		System.out.println("");
		for (Word w : word)
			w.printText();
		for (Word w : word)
			w.encodeText();
		System.out.println("These are the encoded words: ");
		System.out.println("");
		for (Word w : word)
			w.printText();

	}
}