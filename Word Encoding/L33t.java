package wordEncoding;

public class L33T implements Word {

	public String user_text;

	public L33T() {
	}

	// additional constructor for creating objects with text in Main

	public L33T(String user_text) {
		this.user_text = user_text;
	}

	// getter and setter methods

	public void setText(String user_text) {
		this.user_text = user_text;
	}

	public String getText() {
		return user_text;
	}

	// prints updated text

	public void printText() {
		System.out.println(user_text);
	}

	public void encodeText() {

		// library function to easily replace characters in a string
		
		String encoded_word = user_text.replace('e', '3');
		user_text = encoded_word;
	}

	public void decodeText() {
		user_text = user_text.replace('3', 'e');
	}
}