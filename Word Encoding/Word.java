package wordEncoding;

public interface Word {

	public void setText(String user_text);

	public String getText();

	public void printText();

	public void encodeText();

	public void decodeText();

}